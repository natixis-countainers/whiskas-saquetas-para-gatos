# Webcounter Project
Simple Python Webcounter with redis server

## Project tree

```
📦webcounter
 ┣ 📂static
 ┃ ┗ 📜main.css
 ┣ 📂templates
 ┃ ┗ 📜index.html
 ┣ 📜__init__.py
 ┗ 📜__main__.py
```

---
## Developer tasks

### Local run

    $ python -m webcounter

### Local test
    
    $ python -m pytest tests/

### Coverage Report

    coverage run --source=webcounter -m pytest tests
    coverage report -m

---
## Manual server operations

### Build
    docker build -t gomesalexandre/webcounter:latest .

### Run Dependencies
    docker run -d  -p 6379:6379 --name redis --rm redis:alpine

### Deploy
    docker run -d --rm -p 80:5000 --name webcounter --link redis -e REDIS_URL=redis gomesalexandre/webcounter:latest

---
## Cluster operations

### Push to docker repository

    docker login 
    docker push gomesalexandre/webcounter:latest

### Create a docker swarm cluster

    docker swarm init

### Deploy stack app 

    docker stack deploy --compose-file docker-compose.yml app

---
## Automated operations CI/CD


### GitLab Variables 
Variables are in: `Gitlab » Settings » CI/CD » Variables`

Create Gitlab variables with docker credentials: `$USER` and `$PASSWORD`
 
### Get gitlab token and change `--registration-token` bellow

Token are in: `Gitlab » Settings » CI/CD » Runners`

### Instal gitlab-runner on managers

    sudo apk add gitlab-runner

 ### Gitlab register at server

Add shell executer for production

    gitlab-runner register -n \
    --url https://gitlab.com/ \
    --executor shell \
    --description "docker-playground" \
    --tag-list "production" \
    --registration-token GR1348941_1U2TsC7ExJfXwRRYFvt

Add docker executer for tests

    gitlab-runner register -n \
    --url https://gitlab.com/ \
    --executor docker \
    --docker-image "python:3.10-alpine" \
    --description "docker-playground" \
    --tag-list "test-server" \
    --registration-token <gitlab-runner-token>

### Run runner

    gitlab-runner run
